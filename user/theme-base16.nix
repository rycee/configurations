{ config, sources, ... }:

let

  inherit (config.lib.theme.base16) fromYamlFile;

  nurNoPkgs = import (import ../nix/sources.nix).nur { };
  # nurNoPkgs.repos.rycee = import ../../nur-expressions { };

in {
  imports = [
    nurNoPkgs.repos.rycee.hmModules.theme-base16
    # ../../nur-expressions/hm-modules/theme-base16
  ];

  theme.base16 = if true then
    fromYamlFile "${sources.base16-tomorrow-scheme}/tomorrow-night.yaml"
  else
    fromYamlFile "${sources.base16-tomorrow-scheme}/tomorrow.yaml";
}
