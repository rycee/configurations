{ config, lib, pkgs, ... }:

with lib;

let

  hmFilesIgnorePatterns = concatStringsSep "\n" # \
    (map (file: "ignore = Path ${file.target}") # \
      (filter (file: !file.recursive) # \
        (mapAttrsToList (_: file: file) config.home.file))) + "\n";

in {
  options = {
    rycee.sync = {
      enable = mkEnableOption "home directory synchronization";

      target = mkOption {
        type = types.str;
        description = "Where the home directory should be synchronized.";
      };
    };
  };

  config = mkIf config.rycee.sync.enable {
    home.file.".unison/sync-to-${config.rycee.sync.target}.prf".text = ''
      # No need to watch.
      watch = false

      # No loggin, please.
      logfile = /dev/null

      # A bit of safety.
      confirmbigdeletes = true
      confirmmerge = true

      # Diffing and merging.
      #diff = diff -y -W 79 --suppress-common-lines
      merge = Name * -> emacs --eval '(ediff-merge-files "CURRENT1" "CURRENT2" nil "NEW")'

      # The relevant roots.
      root = ${config.home.homeDirectory}
      root = ssh://${config.rycee.sync.target}/${config.home.homeDirectory}

      # What I don't want.
      ignore = Name *.hi
      ignore = Name *.nobackup
      ignore = Name *.o
      ignore = Name *~
      ignore = Name .*~
      ignore = Name .compose-cache
      ignore = Name .mozilla/extensions
      ignore = Path .Xauthority
      ignore = Path .config/syncthing
      ignore = Path .dbus
      ignore = Path .gnupg/reader_*.status
      ignore = Path .local/share/autojump/autojump.txt
      ignore = Path .nix-defexpr
      ignore = Path .nix-profile
      ignore = Path .ssh/master-*
      ignore = Path .unison/home-root.prf
      ignore = Path .unison/sync-to-*.prf
      ignore = Path .unison/{ar*,fp*,*.log,l*}
      ignore = Path .xmonad

      ignore = Regex .cache/.*
      ignore = Regex .*/.cache/.*

      ignore = Path .config/nixpkgs/home.nix
      ignore = Path .config/nixpkgs/overlays/nixpkgs.nix
      ignore = Path .emacs.d/secrets.el

      ${hmFilesIgnorePatterns}

      # Share borg cache to avoid issues with conflicting updates.
      ignorenot = BelowPath .cache/borg
    '';
  };
}
