{ config, pkgs, lib, ... }:

let

  colors = config.theme.base16.colors;

  rgb = base: "#${base.hex.rgb}";

  rgba = base: alpha:
    "rgba(${
      lib.concatMapStringsSep ", " toString [
        base.dec.r
        base.dec.g
        base.dec.b
        alpha
      ]
    })";

  rgbI = base: rgb base + " !important";
  rgbaI = base: alpha: rgba base alpha + " !important";

in {
  programs.firefox = {
    extensions = with pkgs.nur.repos.rycee.firefox-addons; [
      anchors-reveal
      auto-tab-discard
      browserpass
      cookie-autodelete
      darkreader
      https-everywhere
      link-cleaner
      linkhints
      localcdn
      offline-qr-code-generator
      privacy-badger
      reddit-enhancement-suite
      swedish-dictionary
      tree-style-tab
      tst-tab-search
      ublock-origin
    ];

    profiles = {
      default = {
        isDefault = true;
        settings = {
          "beacon.enabled" = false;
          "browser.contentblocking.category" = "strict";
          "browser.display.background_color" = "#c5c8c6";
          "browser.display.foreground_color" = "#1d1f21";
          "browser.download.dir" = "${config.home.homeDirectory}/download";
          "browser.safebrowsing.appRepURL" = "";
          "browser.safebrowsing.malware.enabled" = false;
          "browser.search.hiddenOneOffs" =
            "Google,Yahoo,Bing,Amazon.com,Twitter";
          "browser.search.suggest.enabled" = false;
          "browser.send_pings" = false;
          "browser.startup.page" = 3;
          "browser.tabs.closeWindowWithLastTab" = false;
          "browser.uidensity" = 1; # Dense.
          "browser.urlbar.placeholderName" = "DuckDuckGo";
          "browser.urlbar.speculativeConnect.enabled" = false;
          "devtools.theme" = "${config.theme.base16.kind}";
          "dom.battery.enabled" = false;
          "dom.event.clipboardevents.enabled" = false;
          "dom.security.https_only_mode" = true;
          "experiments.activeExperiment" = false;
          "experiments.enabled" = false;
          "experiments.supported" = false;
          "extensions.pocket.enabled" = false;
          "general.smoothScroll" = false;
          "geo.enabled" = false;
          "gfx.webrender.all" = true;
          "layout.css.devPixelsPerPx" = "1";
          "media.navigator.enabled" = false;
          "media.video_stats.enabled" = false;
          "network.IDN_show_punycode" = true;
          "network.allow-experiments" = false;
          "network.dns.disablePrefetch" = true;
          "network.http.referer.XOriginPolicy" = 2;
          "network.http.referer.XOriginTrimmingPolicy" = 2;
          "network.http.referer.trimmingPolicy" = 1;
          "network.prefetch-next" = false;
          "permissions.default.shortcuts" = 2; # Don't steal my shortcuts!
          "privacy.donottrackheader.enabled" = true;
          "privacy.donottrackheader.value" = 1;
          "privacy.firstparty.isolate" = true;
          "signon.rememberSignons" = false;
          "toolkit.legacyUserProfileCustomizations.stylesheets" = true;
          "widget.content.gtk-theme-override" = "Adwaita:light";
        };

        userChrome = ''
          @namespace url("http://www.mozilla.org/keymaster/gatekeeper/there.is.only.xul");

          #TabsToolbar {
            visibility: collapse;
          }

          #titlebar {
            display: none;
          }

          #sidebar-header {
            display: none;
          }
        '';

        userContent = ''
          @-moz-document url(about:home), url(about:newtab) {
            body {
              --newtab-background-color: ${rgbI colors.base00};
              --newtab-element-hover-color: ${rgbI colors.base01};
              --newtab-icon-primary-color: ${rgbaI colors.base04 0.4};
              --newtab-search-border-color: ${rgbaI colors.base01 0.2};
              --newtab-search-dropdown-color: ${rgbI colors.base00};
              --newtab-search-dropdown-header-color: ${rgbI colors.base00};
              --newtab-search-icon-color: ${rgbaI colors.base04 0.4};
              --newtab-section-header-text-color: ${rgbI colors.base05};
              --newtab-snippets-background-color: ${rgbI colors.base01};
              --newtab-text-primary-color: ${rgbI colors.base05};
              --newtab-textbox-background-color: ${rgbI colors.base01};
              --newtab-textbox-border: ${rgbaI colors.base01 0.2};
              --newtab-topsites-background-color: ${rgbI colors.base04};
              --newtab-topsites-label-color: ${rgbI colors.base05};
            }
          }
        '';
      };
    };
  };

  programs.browserpass = {
    enable = config.programs.firefox.enable;
    browsers = [ "firefox" ];
  };
}
