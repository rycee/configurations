module Main where

import Graphics.X11.ExtraTypes.XF86
import XMonad
import XMonad.Actions.CopyWindow (copyToAll, killAllOtherCopies)
import qualified XMonad.Actions.CycleWS as C
import XMonad.Config.Desktop (desktopConfig, desktopLayoutModifiers)
import XMonad.Hooks.EwmhDesktops (ewmh)
import XMonad.Hooks.ManageHelpers (doFullFloat, isFullscreen)
import XMonad.Hooks.Place (placeHook)
import qualified XMonad.Hooks.Place as Place
import XMonad.Layout.NoBorders (noBorders, smartBorders)
import XMonad.Util.EZConfig (additionalKeys, removeKeys)

meta :: KeyMask
meta = mod4Mask

colActive, colInactive :: String
colActive = "#38556d"
colInactive = "#383d3f"

removedKeys :: [(KeyMask, KeySym)]
removedKeys =
  [ -- Having xmonad restart on M-q is annoying since I accidentally press this
    -- key combination all the time!
    (meta, xK_q)
  ]

addedKeys :: [((KeyMask, KeySym), X ())]
addedKeys =
  [ ((meta, xK_Right), C.moveTo C.Next C.NonEmptyWS),
    ((meta, xK_Left), C.moveTo C.Prev C.NonEmptyWS),
    ((meta, xK_r), C.toggleWS),
    ((meta, xK_e), spawn "xset s activate"),
    ((meta, xK_m), musicTogglePauseCmd),
    -- Various rofi modes.
    ((meta, xK_p), spawn "rofi -show run"),
    ((meta .|. shiftMask, xK_p), spawn "rofi -show window"),
    ((meta, xK_i), spawn "rofi -show emoji -modi emoji"),
    -- To make focused window always visible and to toggle it back to normal.
    ((meta, xK_v), windows copyToAll),
    ((meta .|. shiftMask, xK_v), killAllOtherCopies),
    -- Restart XMonad using a more convoluted method.
    ((meta .|. controlMask, xK_q), restart "xmonad" True),
    ((0, xK_Print), spawn "flameshot gui"),
    -- Hook up media keys.
    ((0, xF86XK_AudioRaiseVolume), spawn "dvol -i 2"),
    ((0, xF86XK_AudioLowerVolume), spawn "dvol -d 2"),
    ((0, xF86XK_AudioMute), spawn "dvol -t"),
    ((0, xF86XK_AudioPlay), musicTogglePauseCmd),
    ((0, xF86XK_AudioMicMute), spawn "amixer set Capture toggle"),
    ((0, xF86XK_Display), spawn "autorandr --change --default horizontal"),
    ((0, xF86XK_MonBrightnessUp), spawn "dlight -T 1.15"),
    ((0, xF86XK_MonBrightnessDown), spawn "dlight -T 0.85")
  ]
  where
    musicTogglePauseCmd = spawn "rhythmbox-client --play-pause"

layouts =
  desktopLayoutModifiers $
    smartBorders tiled ||| noBorders Full
  where
    tiled = Tall 1 0.03 0.55

manages =
  placeHook (Place.inBounds (Place.smart (0.5, 0.5)))
    <+> manageHook desktopConfig
    <+> (className =? "Gxmessage" --> doFloat)
    <+> (className =? "mplayer2" --> doFloat)
    <+> (className =? "mpv" --> doFloat)
    <+> (className =? "peek" --> doFloat)
    <+> (title =? "udiskie" --> doFloat)
    <+> (isFullscreen --> doFullFloat)

xmonadConfig =
  desktopConfig
    { terminal = "gnome-terminal",
      modMask = meta,
      workspaces = map show [1 .. 9],
      borderWidth = 1,
      normalBorderColor = colInactive,
      focusedBorderColor = colActive,
      manageHook = manages,
      layoutHook = layouts
    }
    `removeKeys` removedKeys
    `additionalKeys` addedKeys

main :: IO ()
main = xmonad . ewmh $ xmonadConfig
