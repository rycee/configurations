let

  inherit (builtins) elem parseDrvName;

in {
  allowUnfreePredicate = pkg:
    elem (pkg.pname or (parseDrvName pkg.name)) [
      "android-sdk"
      "skypeforlinux"
      "vanilla-dmz"
    ];

  android_sdk.accept_license = true;

  allowAliases = false;
  allowBroken = false;
}
