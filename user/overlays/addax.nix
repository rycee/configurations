self: super: {
  haskellPackages = super.haskellPackages.override {
    overrides = self: super: {
      brick-widgets-pandoc = super.callPackage ~/devel/brick-widgets-pandoc { };
      addax = super.callPackage ~/devel/addax { };
    };
  };
}
