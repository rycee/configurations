self: super:

let

  sources = import ~/devel/configurations/nix/sources.nix;

in {
  rah.sources = sources;
  nixpkgs-unstable = import sources.nixpkgs-unstable { overlays = [ ]; };
  nur = import sources.nur { pkgs = self; };
}
