{ config, pkgs, lib, sources, ... }:

let

  inherit (lib) mkDefault mkForce mkIf mkMerge optionals;

  tasks = config.rycee.tasks;

  homeDirectory = config.home.homeDirectory;

  ytdFormat = h:
    "bestvideo[height<=?${toString h}][fps<=?30]+bestaudio/best[height<=?${
      toString h
    }][fps<=?30]";

in {
  imports = [
    ./emacs.nix
    ./firefox.nix
    ./secrets.nix
    ./sync.nix
    ./tasks.nix
    ./theme-base16.nix
  ];

  _module.args.sources = import ../nix/sources.nix;

  fonts.fontconfig.enable = mkDefault tasks.desktop;

  nixpkgs = {
    config = import dotfiles/nixpkgs-config.nix;
    overlays = [
      (self: super: {
        nixpkgs-unstable = import sources.nixpkgs-unstable { overlays = [ ]; };
        nur = import sources.nur { pkgs = self; };

        inherit (self.nixpkgs-unstable) mcfly;
      })
    ];
  };

  # Disable the version consistency check because I often use unstable Home
  # Manager with stable Nixpkgs. This is not recommended unless you are really
  # careful.
  home.enableNixpkgsReleaseCheck = false;

  home.packages = with pkgs;
    [
      atool
      elinks
      fortune
      hexyl # Command line hex file viewer.
      moreutils
      mtr
      ncdu
      nethogs
      pv
      reptyr
      screen
      unzip
      zip

      # To easily find Nix GC roots that are scattered around.
      nur.repos.rycee.nix-stray-roots

      # Nice dd setup for most cases.
      (writeShellScriptBin "ddi" ''
        ${coreutils}/bin/dd if="$1" of="$2" \
          bs=128k iflag=nocache oflag=direct conv=fsync status=progress
      '')
    ] ++ optionals tasks.desktop [
      # Fonts
      caladea
      carlito
      comic-relief
      dejavu_fonts
      emacs-all-the-icons-fonts
      fantasque-sans-mono
      fira-code
      fira-mono
      font-awesome_5
      inconsolata
      iosevka
      league-of-moveable-type
      libertine
      national-park-typeface
      noto-fonts-emoji
      orbitron
      overpass
      pecita
      quattrocento
      quattrocento-sans
      roboto
      stix-otf
      terminus_font
      ttf_bitstream_vera
      undefined-medium
      unifont
      # (nerdfonts.override { fonts = [ "FantasqueSansMono" ]; })

      # wrk                         # HTTP load tester.

      aspell
      aspellDicts.en
      aspellDicts.sv
      cloc
      colordiff
      cpulimit
      darkhttpd # Minimal HTTP server.
      dmenu
      dos2unix
      dstat
      dtach
      dzen2
      fd
      feh
      figlet
      file
      filegive # To share files.
      git
      git-hub
      git-standup
      gnome.adwaita-icon-theme
      gnome.eog
      gnome.evince
      gnome.gnome-themes-extra
      gnupg
      graphviz-nox
      gtk-engine-murrine
      haskellPackages.ShellCheck
      haskellPackages.bench
      haskellPackages.citeproc
      hicolor-icon-theme
      hunspell
      hunspellDicts.en-us
      jwhois
      lab
      libnotify # For notify-send
      libressl.nc
      lnav # Log file navigator
      lsof
      man-pages
      mimeo
      niv
      nix
      nix-index
      nox
      pandoc
      pdfgrep
      pdftk
      peek # Simple screen recordings.
      powertop
      radeontop
      redshift
      ripgrep
      rsync
      s6-dns
      screen-message
      screenfetch
      scrot
      sd # Search and replace tool.
      simplescreenrecorder
      sl
      speedcrunch
      sqlite-interactive
      strace
      tango-icon-theme
      theme-vertex
      time
      trash-cli
      udiskie
      unison
      units
      usbutils
      wdiff
      wireshark
      xclip
      xdg-utils
      xh
      xml2
      yq

      (pass.withExtensions (exts: [ exts.pass-otp ]))

      (import (fetchFromGitHub {
        owner = "Shopify";
        repo = "comma";
        rev = "4a62ec17e20ce0e738a8e5126b4298a73903b468";
        sha256 = "0n5a3rnv9qnnsrl76kpi6dmaxmwj1mpdd2g0b4n1wfimqfaz6gi1";
      }) { inherit pkgs; })

      (runCommandLocal "rah-utils" { } ''
        for tool in ${./bin}"/"*; do
          install -D -m755 $tool $out/bin/$(basename $tool)
        done

        substituteInPlace $out/bin/dlight \
          --subst-var-by path ${lib.makeBinPath [ libnotify light ]}

        substituteInPlace $out/bin/dvol \
          --subst-var-by path ${
            lib.makeBinPath [ alsaUtils coreutils gnused libnotify ]
          }

        patchShebangs $out/bin
      '')
    ] ++ optionals (tasks.desktop && !tasks.container) [
      # brasero
      # devede
      # eventstat
      # lightlocker
      # octave
      # qgis
      # subtitleeditor
      anki
      arandr
      autorandr
      bibtool
      borgbackup
      cachix
      calibre
      darcs
      darktable
      desktop-file-utils
      electron-cash
      electrum
      ffmpeg
      fgallery
      flac
      gcr
      gimp
      git-crypt
      gnome.cheese
      gnome.dconf-editor
      gnome.file-roller
      gnome.gedit
      gnome.gnome-calculator
      gnome.gnome-font-viewer
      gnome.gnome-keyring
      gnome.gucharmap
      gnome.libgnome-keyring
      gnome.seahorse
      gparted
      gpsprune
      inkscape
      jhead
      jmtpfs
      josm
      ledger-live-desktop
      libreoffice
      libsecret
      monero
      ncmpcpp
      neomutt
      nix-prefetch-scripts
      nixops
      nixpkgs-lint
      notmuch
      openambit
      pavucontrol
      pciutils
      pdf2svg
      psmisc
      pytrainer
      scantailor
      shotwell
      skypeforlinux
      sshfs-fuse
      stellarium
      svtplay-dl
      tesseract # OCR
      thunderbird
      transmission-gtk
      unpaper
      urlwatch
      vdirsyncer
      which
      whipper # CD ripper
      xorg.xmodmap
      youtube-dl

      (writeShellScriptBin "skype" ''
        skypeforlinux --force-device-scale-factor=1
      '')
    ] ++ optionals (tasks.desktop && tasks.music) [
      (rhythmbox.override {
        gst_plugins = with gst_all_1; [
          gst-plugins-good
          gst-plugins-bad
          gst-plugins-ugly
        ];
      })
    ] ++ optionals tasks.docker [
      # Dive is a handy TUI application for exploring the content of Docker
      # images.
      dive
      docker-compose
    ] ++ optionals tasks.dev.haskell [
      cabal2nix

      # For easy access to a ghci with some handy packages.
      (haskellPackages.ghcWithHoogle (hpkgs: [
        hpkgs.lens
        hpkgs.pretty-simple
        hpkgs.random
        hpkgs.text
        hpkgs.turtle
      ]))
    ] ++ optionals tasks.dev.java [
      groovy
      jdk11
      lombok
      visualvm

      (maven.override { jdk = jdk11; })
    ] ++ optionals tasks.dev.perl [
      nix-generate-from-cpan
      perlPackages.PerlCritic
      perlPackages.PerlTidy
    ] ++ optionals tasks.dev.rust [
      (stdenvNoCC.mkDerivation {
        name = "cargo-completions";
        src = cargo.src;
        preferLocalBuild = true;
        allowSubstitutes = false;
        nativeBuildInputs = [ installShellFiles ];
        dontConfigure = true;
        dontBuild = true;
        installPhase = ''
          installShellCompletion --bash --name cargo \
            src/tools/cargo/src/etc/cargo.bashcomp.sh
        '';
      })
    ];

  # Setup language. Want mostly American English but with some
  # customization.
  home.language = {
    base = "en_US.UTF-8";
    address = "sv_SE.UTF-8";
    monetary = "sv_SE.UTF-8";
    paper = "en_DK.UTF-8";
    time = "en_DK.UTF-8";
  };

  home.keyboard = {
    layout = mkDefault "us,bg";
    variant = mkDefault ",phonetic";
    options = [
      "ctrl:nocaps"
      "altwin:no_win"
      "compose:menu"
      "grp:shifts_toggle"
      "grp_led:caps"
    ];
  };

  home.sessionVariables = mkMerge [
    {
      # Don't let python spew pyc-files all over.
      PYTHONDONTWRITEBYTECODE = "foo";

      # Prepare for Java.
      _JAVA_OPTIONS = "-Dawt.useSystemAAFontSettings=on";
      _JAVA_AWT_WM_NONREPARENTING = "1"; # I prefer non-gray windows.

      EDITOR = "emacs";

      GS_OPTIONS = "-sPAPERSIZE=a4";

      # Add hunspell dictionaries under NixOS.
      DICPATH = "$HOME/.nix-profile/share/hunspell";

      # In GTK+ programs I always want XIM.
      GTK_IM_MODULE = "xim";

      # Colorific GCC.
      GCC_COLORS =
        "error=01;31:warning=01;35:note=01;36:caret=01;32:locus=01:quote=01";

      # Colorific less.
      LESS = "-R -X";

      # Colorific darcs whatsnew.
      DARCS_DO_COLOR_LINES = 1;

      REQUESTS_CA_BUNDLE = "/etc/ssl/certs/ca-bundle.crt";
    }

    (mkIf config.programs.mcfly.enable { MCFLY_INTERFACE_VIEW = "BOTTOM"; })

    (mkIf tasks.container {
      # We always log in through Gnome Terminal so can handle fancy colors.
      TERM = "xterm-256color";
    })

    (mkIf tasks.dev.java {
      MAVEN_OPTS = "-Xmx1g -XX:+CMSClassUnloadingEnabled";
    })
  ];

  # Some extra dotfiles that don't have their own modules.
  home.file = mkMerge [
    { ".screenrc".source = dotfiles/screenrc; }

    (mkIf (tasks.desktop && !tasks.container) {
      ".XCompose".source = dotfiles/xcompose;
      ".thunderbird/8q39nq47.default/chrome/userChrome.css".source =
        dotfiles/thunderbird/userChrome.css;
      ".thunderbird/8q39nq47.default/chrome/userContent.css".source =
        dotfiles/thunderbird/userContent.css;
    })

    (mkIf tasks.dev.haskell { ".ghc/ghci.conf".source = dotfiles/ghci; })

    (mkIf tasks.dev.java {
      ".gradle/gradle.properties".source = dotfiles/gradle.properties;
    })
  ];

  gtk = {
    enable = tasks.desktop;
    font = {
      package = pkgs.dejavu_fonts;
      name = "DejaVu Sans 8";
    };

    gtk2.extraConfig = ''
      gtk-recent-files-max-age = 0
      gtk-cursor-blink = 0
    '';

    gtk3.bookmarks = [ "file://${config.xdg.userDirs.download}" ];

    gtk3.extraConfig = {
      gtk-recent-files-max-age = 0;
      gtk-recent-files-limit = 0;
      gtk-cursor-blink = false;
    };

    gtk3.extraCss = ''
      @binding-set NoKeyboardNavigation {
          unbind "<shift>F10"
      }

      * {
          gtk-key-bindings: NoKeyboardNavigation
      }

      GtkToolbar#swt-toolbar-flat {
          padding: 0px;
      }
    '';
  };

  qt = {
    enable = tasks.desktop;
    platformTheme = "gtk";
  };

  manual = {
    html.enable = mkDefault tasks.desktop;
    json.enable = mkDefault tasks.desktop;
    manpages.enable = mkDefault tasks.desktop;
  };

  programs.bat.enable = mkDefault true;
  programs.dircolors.enable = mkDefault true;
  programs.emacs.enable = mkDefault tasks.desktop;
  programs.firefox.enable = mkDefault tasks.desktop;
  programs.info.enable = mkDefault true;
  programs.lesspipe.enable = mkDefault true;
  programs.mcfly.enable = mkDefault tasks.desktop;

  programs.bash = {
    enable = true;

    shellAliases = mkMerge [
      {
        # Make life a bit less stressful.
        mv = "mv -i";
        cp = "cp -i";
        rm = "rm -I";

        ls = mkDefault "ls --color=auto";
        l = mkDefault "ls -l";
        ll = mkDefault "ls -Al";
        ".." = "cd ..";
        "..." = "cd ../..";
        x = "exit";
        e = "$EDITOR";
        ew = "emacs -nw";

        # Hashing.
        h256 = "sha256sum";
        h512 = "sha512sum";

        # Colorific grep.
        grep = "grep --color=auto";
        egrep = "egrep --color=auto";

        # Runs bat without line numbers and wrapping.
        rat = "bat --style=plain --wrap=never";

        # Shortcut for a universal open command.
        o = "${pkgs.mimeo}/bin/mimeo";

        # Shorter trash command.
        t = "trash";

        hm = "home-manager";
      }

      (mkIf tasks.desktop {
        # Video download should prefer 480p and max download rate is 2 MiB/s.
        ytd = ''youtube-dl -f "${ytdFormat 480}" -r2m'';

        # Video download that prefers 720p and max download rate is 2 MiB/s.
        ytd720 = ''youtube-dl -f "${ytdFormat 720}" -r2m'';

        mpv720 = "mpv --ytdl-format='${ytdFormat 720}'";
      })

      (mkIf tasks.dev.haskell {
        # Avoid eating the whole system on space leak.
        ghci = "ghci +RTS -M500m -RTS";
      })
    ];

    shellOptions =
      lib.mkOptionDefault [ "cdspell" "dirspell" "histreedit" "histverify" ];

    historyFileSize = 500000;
    historySize = 500000;

    historyControl = [ "erasedups" "ignorespace" ];

    historyIgnore = [ "l" "x" "exit" "bg" "fg" "history" "ls" "cd" ".." "..." ];

    # Need to be after starship init since it overwrites PROMPT_COMMAND.
    initExtra = lib.mkAfter ''
      # Set a soft memory limit on processes to 8 GB.
      ulimit -S -m 8000000

      ${lib.optionalString (!config.programs.mcfly.enable) ''
        PROMPT_COMMAND="''${PROMPT_COMMAND:+''${PROMPT_COMMAND/%;*( )};}history -a"
        HISTTIMEFORMAT='%F %T '
      ''}

      # Must C-d at least thrice to close shell.
      export IGNOREEOF=2
    '';
  };

  programs.beets = {
    enable = tasks.music;
    package = pkgs.beets.override {
      enableAura = false;
      enableEmbyupdate = false;
      enableKeyfinder = false;
      enableKodiupdate = false;
      enableLastfm = false;
      enableSonosUpdate = false;
      enableSubsonicplaylist = false;
      enableSubsonicupdate = false;
    };
    settings = {
      directory = "${homeDirectory}/music";
      library = "${homeDirectory}/.local/share/beets/musiclibrary.blb";
      plugins = "info chroma fetchart embedart mbsync scrub convert bpd";

      ui = { color = true; };

      "import" = {
        move = true;
        languages = "en";
        log = "/tmp/${config.home.username}-beetslog.txt";
      };

      match = { preferred = { original_year = true; }; };

      bpd = { host = "127.0.0.1"; };

      convert = {
        threads = 2;
        max_bitrate = 200;
        dest = "${homeDirectory}/sync/music";
        format = "ogg";
      };
    };
  };

  programs.direnv = {
    enable = mkDefault tasks.desktop;
    nix-direnv.enable = true;
    stdlib = ''
      declare -A direnv_layout_dirs
      direnv_layout_dir() {
          local cache_dir="''${XDG_CACHE_HOME:-$HOME/.cache}"
          echo "''${direnv_layout_dirs[$PWD]:=$(
              echo -n "$cache_dir"/direnv/layouts/
              echo -n "$PWD" | shasum | cut -d ' ' -f 1
          )}"
      }

      use_nix_flake() {
        watch_file flake.nix
        watch_file flake.lock
        eval "$(nix run nixpkgs.nixpkgs-unstable.nixUnstable -c nix --experimental-features 'nix-command flakes' print-dev-env --profile "$(direnv_layout_dir)/flake-profile")"
      }
    '';
  };

  programs.exa = {
    enable = mkDefault true;
    enableAliases = true;
  };

  programs.fzf = {
    enable = mkDefault true;
    fileWidgetCommand = "${pkgs.fd}/bin/fd --type f";
    changeDirWidgetCommand = "${pkgs.fd}/bin/fd --type d";
    defaultOptions = [ "--border" "--inline-info" ];
  };

  programs.git = {
    userName = "Robert Helgesson";
    aliases = {
      count = "rev-list --all --count";
      snapshot =
        ''!git stash save "snapshot: $(date)" && git stash apply "stash@{0}"'';
    };
    extraConfig = {
      core.whitespace = "trailing-space,space-before-tab";
      pull.ff = "only";
      rerere.enabled = true;
    };
  };

  programs.gnome-terminal = {
    enable = mkDefault (tasks.desktop && !tasks.container);
    showMenubar = false;
    profile = {
      "5ddfe964-7ee6-4131-b449-26bdd97518f7" = {
        default = true;
        cursorShape = "ibeam";
        font = mkDefault "Fantasque Sans Mono 8";
        showScrollbar = false;
        scrollOnOutput = false;
      };
    };
  };

  programs.home-manager = mkMerge [
    { enable = mkDefault true; }

    (mkIf (tasks.desktop && !tasks.container) {
      path = "${homeDirectory}/devel/home-manager";
    })
  ];

  programs.htop = {
    enable = true;
    settings.hide_threads = true;
  };

  programs.jq = {
    enable = true;
    enableBase16Theme = true;
  };

  programs.mpv = {
    enable = mkDefault tasks.desktop;

    bindings = {
      MOUSE_BTN0 = "ignore";
      MOUSE_BTN1 = "ignore";
      MOUSE_BTN2 = "cycle pause";
      MOUSE_BTN3 = "ignore";
      MOUSE_BTN4 = "ignore";
      MOUSE_BTN5 = "ignore";
      MOUSE_BTN6 = "ignore";
    };

    config = {
      hwdec = "auto";

      stop-screensaver = true;

      # 480p is sufficient in most cases.
      ytdl-format = ytdFormat 480;

      # To not have squeeky voices.
      af-add = "scaletempo";
    };
  };

  programs.readline = {
    enable = true;
    variables = {
      # Don't show hidden files unless a '.' is prefixed.
      match-hidden-files = false;

      # Expand tilde to home directory.
      expand-tilde = true;

      # Improve completion usability.
      completion-ignore-case = true;
      completion-prefix-display-length = 2;
      completion-map-case = true;

      # Avoid pressing TAB so much.
      show-all-if-ambiguous = true;
      show-all-if-unmodified = true;

      # Indicate file types.
      visible-stats = true;

      # Disable the internal pager.
      page-completions = false;
    };
  };

  programs.rofi = {
    enable = tasks.desktop && !tasks.container;
    package = pkgs.rofi.override { plugins = [ pkgs.rofi-emoji ]; };
  };

  programs.ssh = {
    enable = mkDefault tasks.desktop;
    controlMaster = "auto";
    controlPersist = "5m";
  };

  programs.starship = {
    enable = true;
    settings = {
      cmd_duration.disabled = true;
      git_status.disabled = true;
    };
  };

  programs.texlive = {
    enable = tasks.desktop;
    extraPackages = tpkgs: {
      inherit (tpkgs)
        collection-fontsrecommended collection-latexrecommended
        collection-mathscience collection-pictures collection-xetex

        # Languages.
        babel-swedish hyphen-swedish

        # Fonts.
        bera # E.g. the beramono font.
        mathdesign

        # Extra packages.
        capt-of moderncv relsize ticket ulem wrapfig;
    };
  };

  services.keepassx.enable = mkDefault (tasks.desktop && !tasks.container);
  services.network-manager-applet.enable =
    mkDefault (tasks.desktop && !tasks.container);
  services.syncthing.enable = mkDefault (tasks.desktop && !tasks.container);
  services.udiskie.enable = mkDefault (tasks.desktop && !tasks.container);

  services.gpg-agent = {
    defaultCacheTtl = 1800;
    enableSshSupport = true;
  };

  services.flameshot = {
    enable = mkDefault (tasks.desktop && !tasks.container);
    settings = {
      General = {
        disabledTrayIcon = true;
        showStartupLaunchMessage = false;
      };
    };
  };

  services.redshift = {
    enable = mkDefault (tasks.desktop && !tasks.container);

    # # Lund, Sweden
    # latitude = "55.703";
    # longitude = "13.193";

    # Sofia, Bulgaria.
    latitude = "42.698";
    longitude = "23.322";
  };

  services.dunst = {
    enable = mkDefault tasks.desktop;
    iconTheme = {
      package = pkgs.gnome.adwaita-icon-theme;
      name = "Adwaita";
    };
    settings = {
      global = {
        browser = "${config.programs.firefox.package}/bin/firefox -new-tab";
        dmenu = "${pkgs.rofi}/bin/rofi -dmenu";
        follow = "mouse";
        font = "DejaVu Sans 9";
        format = "<b>%s</b>\\n%b";
        frame_width = 2;
        geometry = "500x5-5+30";
        horizontal_padding = 8;
        icon_position = "left";
        max_icon_size = 32;
        line_height = 0;
        markup = "full";
        padding = 8;
        separator_height = 2;
        transparency = 10;
        word_wrap = true;
      };

      urgency_low.timeout = 15;
      urgency_normal.timeout = 20;
      urgency_critical.timeout = 0;

      shortcuts = {
        context = "mod4+grave";
        close = "mod4+shift+space";
      };
    };
  };

  services.gnome-keyring = {
    enable = mkDefault tasks.desktop;
    components = [ "secrets" "pkcs11" ];
  };

  services.polybar = {
    enable = tasks.desktop && !tasks.container;
    script = ''
      {
        until ${pkgs.procps}/bin/pidof xmonad-x86_64-linux ; do
          ${pkgs.coreutils}/bin/sleep 0.5
        done
        polybar top
      } &
    '';
    config = {
      "bar/top" = {
        modules-left = toString [ "xworkspaces" "xwindow" ];
        modules-right = toString [
          "pub-ip"
          "mail-count"
          "wireless-network"
          "wired-network"
          "cpu"
          "memory"
          "battery"
          "keyboard"
          "date"
        ];
        module-margin = 1;
        background = "\${colors.background}";
        foreground = "\${colors.foreground}";
        font-0 = "Fantasque Sans Mono:size=12;3";
        font-1 = "Font Awesome 5 Free:style=Solid:size=11;4";
        width = "100%";
        height = 26;
        tray-position = "right";
      };

      "module/pub-ip" = let
        fetchIp = "${pkgs.curl}/bin/curl -s https://ipinfo.io/json";
        jq = "${pkgs.jq}/bin/jq";
        xclip = "${pkgs.xclip}/bin/xclip";
      in {
        type = "custom/script";
        interval = 300;
        exec = toString (pkgs.writeShellScript "pub-ip" ''
          ${fetchIp} | ${jq} -r '"[\(.ip) \(.country)]"'
        '');
        click-left = toString (pkgs.writeShellScript "pub-ip" ''
          ${fetchIp} | ${jq} -r '"\(.ip)"' | ${xclip} -rmlastnl
        '');
      };

      "module/mail-count" = let
        notmuch = "${pkgs.notmuch}/bin/notmuch";
      in {
        type = "custom/script";
        interval = 30;
        exec = toString (pkgs.writeShellScript "mail-count" ''
          export NOTMUCH_CONFIG="${config.home.sessionVariables.NOTMUCH_CONFIG}"
          count=$(${notmuch} count tag:unread)
          if (( $count > 0 )); then
            echo -n " $count"
          fi
        '');
      };

      "module/date" = {
        type = "internal/date";
        date = "%a %b %d";
        time = "%H:%M";
        label = "%date% %time%";
        rate = 5;
      };

      "module/keyboard" = {
        type = "internal/xkeyboard";
        format = "<label-layout>";
      };

      "module/xworkspaces" = { type = "internal/xworkspaces"; };

      "module/xwindow" = {
        type = "internal/xwindow";
        label = "%title%";
        label-maxlen = 50;
      };

      "module/battery" = {
        type = "internal/battery";
        battery = "BAT0";
        adapter = "AC";
        full-at = 80;
        poll-interval = 5;
        time-format = "%H:%M";
        format-charging = "<animation-charging> <label-charging>";
        label-charging = "%time%";
        format-discharging = "<ramp-capacity> <label-discharging>";
        label-discharging = "%time%";
        format-full = "<ramp-capacity>";
        ramp-capacity-0 = "";
        ramp-capacity-1 = "";
        ramp-capacity-2 = "";
        ramp-capacity-3 = "";
        ramp-capacity-4 = "";
        animation-charging-0 = "";
        animation-charging-1 = "";
        animation-charging-2 = "";
        animation-charging-3 = "";
        animation-charging-4 = "";
        animation-charging-framerate = 750;
      };

      "module/wireless-network" = {
        type = "internal/network";
        interval = 3;
        format-connected = "<label-connected>";
        format-connected-prefix = "";
        label-connected = "%upspeed:8% %downspeed:8%";
        format-disconnected = "";
      };

      "module/wired-network" = {
        type = "internal/network";
        interval = 3;
        format-connected = "<label-connected>";
        format-connected-prefix = "";
        label-connected = "%upspeed:8% %downspeed:8%";
        format-disconnected = "";
      };

      "module/memory" = {
        type = "internal/memory";
        format = "<label> <ramp-used>";
        label = "";
        ramp-used-0 = "▁";
        ramp-used-1 = "▂";
        ramp-used-2 = "▃";
        ramp-used-3 = "▄";
        ramp-used-4 = "▅";
        ramp-used-5 = "▆";
        ramp-used-6 = "▇";
        ramp-used-7 = "█";
      };

      "module/cpu" = {
        type = "internal/cpu";
        format = "<label> <ramp-load>";
        label = "";
        ramp-load-0 = "▁";
        ramp-load-1 = "▂";
        ramp-load-2 = "▃";
        ramp-load-3 = "▄";
        ramp-load-4 = "▅";
        ramp-load-5 = "▆";
        ramp-load-6 = "▇";
        ramp-load-7 = "█";
      };

      "module/pulseaudio" = {
        type = "internal/pulseaudio";
        use-ui-max = true;
        interval = 5;
        format-volume = "<ramp-volume> <label-volume>";
        format-muted = "<label-muted>";
        label-muted = "🔇";
        label-muted-foreground = "#666";
        ramp-volume-0 = "🔈";
        ramp-volume-1 = "🔉";
        ramp-volume-2 = "🔊";
      };
    };
  };

  services.random-background = {
    enable = mkDefault (tasks.desktop && !tasks.container);
    imageDirectory = "%h/backgrounds";
    interval = "1h";
  };

  services.screen-locker = {
    enable = tasks.desktop && !tasks.container;
    enableBase16Theme = true;
    xautolock.enable = false;
    xss-lock.extraOptions = let
      notify = pkgs.writeShellScript "notify-lock" ''
        ${pkgs.libnotify}/bin/notify-send --urgency low --expire-time=5000 -- 'Locking screen in 30 seconds'
      '';
    in [ "-n ${notify}" ];
  };

  systemd.user = {
    startServices = "sd-switch";
    services = mkIf (tasks.desktop && !tasks.container) {
      xss-lock.Service.ExecStartPre =
        mkForce "${pkgs.xorg.xset}/bin/xset s 570 30";
    };
  };

  xdg = {
    configFile = mkMerge [
      {
        "nixpkgs/config.nix".source = dotfiles/nixpkgs-config.nix;
        "user-dirs.locale".source = dotfiles/user-dirs.locale;
      }

      (mkIf tasks.desktop {
        "nixpkgs/overlays" = {
          source = /home/rycee/devel/configurations/user/overlays;
          recursive = true;
        };
      })

      (mkIf (tasks.desktop && !tasks.container) {
        "autorandr/postswitch".source = dotfiles/autorandr-postswitch;
      })
    ];

    userDirs = let dir = s: "${homeDirectory}/${s}";
    in {
      enable = true;
      desktop = dir "";
      documents = dir "doc";
      download = dir "download";
      music = dir "music";
      pictures = dir "photos";
      publicShare = dir "public";
      templates = dir "templates";
      videos = dir "videos";
    };
  };

  xresources.properties = mkIf tasks.desktop {
    "XTerm*loginShell" = true;
    "XTerm*metaSendsEscape" = true;
    "XTerm*eightBitInput" = false;
    "XTerm*dynamicColors" = true;
    "XTerm*savesLines" = 10000;
    "XTerm*toolBar" = false;
    "XTerm*utf8" = 2;
    "XTerm*faceName" = "dejavu sans mono";
    "XTerm*faceSize" = 8;
    "XTerm*highlightSelection" = true;
    # Helps when selecting URLs and such.
    "XTerm*charClass" = [
      "33:48"
      "35-38:48"
      "39:43"
      "42-47:48"
      "58-59:48"
      "61:48"
      "63-64:48"
      "126:48"
    ];
    "XTerm*cursorColor" = "#ff6347";
    "XTerm*background" = "#${config.theme.base16.colors.base00.hex.rgb}";
    "XTerm*foreground" = "#${config.theme.base16.colors.base05.hex.rgb}";
    "Emacs*toolBar" = 0;
    "Emacs*verticalScrollBars" = "off";
    "Emacs*background" = "#${config.theme.base16.colors.base00.hex.rgb}";
    "Emacs*foreground" = "#${config.theme.base16.colors.base05.hex.rgb}";
  };

  xsession = {
    enable = tasks.desktop && !tasks.container;
    pointerCursor = mkIf tasks.desktop {
      package = pkgs.vanilla-dmz;
      name = "Vanilla-DMZ";
    };
    windowManager.xmonad = {
      enable = config.xsession.enable;
      extraPackages = haskellPackages: [ haskellPackages.xmonad-contrib ];
      config = ./dotfiles/xmonad.hs;
    };
    initExtra = ''
      # Turn off beeps.
      xset -b

      # Set up blanking of screen.
      xset s 570 30
      xset dpms 900 1200 1800
    '';
  };
}
